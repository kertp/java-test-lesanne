import java.util.ArrayList;


public class Client {
	private int bonusPoints;
	private StringBuffer txt = new StringBuffer();
	private int totalSum;
	private ArrayList<Film> rentedFilm = new ArrayList<Film>(); //List that holds all the Film that the client has rented
	final private int PREMIUM_PRICE = 4;
	final private int BASIC_PRICE = 3;

	/**
	 * Setter for bonus points.
	 * 
	 * @param bonusPoints the number of bonus points that the client needs to have 
	 * 
	 */
	public void setBonusPoints(int bonusPoints) throws IllegalArgumentException {
		if(bonusPoints < 0){
			throw new IllegalArgumentException("Bonus points can not be a negative number!");
		}
		this.bonusPoints = bonusPoints;
	}

	/**
	 * Getter for bonus points.
	 * 
	 * @return returns the number of bonus points the client has.
	 * 
	 */
	public int getBonusPoints() {
		return bonusPoints;
	}

	/**
	 * @return returns an ArrayList of films that the client has rented.
	 * 
	 */
	public ArrayList<Film> getRentedFilm() {
		return rentedFilm;
	}
	
	/**
	 * Method for lending a film.
	 * 
	 * @param film the film that the client wants to rent.
	 * 
	 * @param days number of days that the client wants to rent the film.
	 * 
	 */
	public void lendFilm(Film film, int days) throws IllegalArgumentException{
		if(days <= 0){
			throw new IllegalArgumentException("The number of days can not be a negative number!");
		}
		
		if(film.isAvailable()){
			// Adds the film to rentedFilm list, sets it unavailable and initializes sum.
			this.rentedFilm.add(film);
			film.setAvailable(false);
			int sum = 0;
			// Price calculations.
			if(film.getType() == FilmType.NEW){
				sum = PREMIUM_PRICE * days;
				bonusPoints += 2;
			} else if(film.getType() == FilmType.REGULAR){
				if(days <= 3){
					sum = BASIC_PRICE;
				} else{
					sum = BASIC_PRICE + BASIC_PRICE * (days - 3);
				}
				bonusPoints += 1;
			} else{
				if(days <= 5){
					sum = BASIC_PRICE;
				} else{
					sum = BASIC_PRICE + BASIC_PRICE * (days - 5);
				}
				bonusPoints += 1;
			}
			txt.append(film + " " + days + " days " + sum + " EUR \n");
			totalSum += sum;
		} else{
			System.out.println("Film " + film + " is not available!");
			System.out.println();
		}

	}
	/**
	 * Method for renting a film and paying with bonus points.
	 * 
	 * @param film the film that the client wants to rent.
	 * 
	 * @param days number of days that the client wants to rent the film.
	 *            
	 * @param payWithPoints number of days that the client wishes to pay with bonus points
	 * 
	 */
	public void lendFilm(Film film, int days, int payWithPoints) throws IllegalArgumentException{
		if(days <= 0 || payWithPoints <= 0){
			throw new IllegalArgumentException("The number of days can not be a negative number!");
		} else if(days < payWithPoints){
			throw new IllegalArgumentException("The number number of days that the client wishes to pay with bonus points can not be greater than the length of rental!");
		}

		if(film.isAvailable()){
			if(film.getType() == FilmType.NEW){
				if(this.bonusPoints < payWithPoints * 25){
					/* If there are insufficient bonus points, then the program finds out for how many days it is possible to pay with bonus points. */
					payWithPoints = this.bonusPoints/(payWithPoints * 25);
					this.lendFilm(film, days, payWithPoints);
				} else{
					/* If payWithPoints == 0, then continue with normal payment. */
					if(payWithPoints == 0){
						this.lendFilm(film, days);
					} /* If the customer wishes to pay with bonus points for all the days, then just use the bonus points and afterwards add 2 extra bonus points. */
					else if(days - payWithPoints == 0){
						this.rentedFilm.add(film);
						film.setAvailable(false);
						this.setBonusPoints(this.bonusPoints - payWithPoints * 25 + 2);
						this.txt.append(film + " " + days + " days " + payWithPoints * 25 + " bonus points \n" );
					} /* If the customer wishers to pay with bonus points for only a part of the days, then afterwards the program uses the method lendFilm(Film, int). */
					else if(days - payWithPoints > 0){
						this.setBonusPoints(this.bonusPoints - payWithPoints * 25);
						this.txt.append(film + " " + payWithPoints + " days " + payWithPoints * 25 + " bonus points \n");
						this.lendFilm(film, days - payWithPoints);
						this.txt.append("-------------------- \n" + "Total: " + film + " " + days + " days \n \n");
					}
				}
			} else{
				this.lendFilm(film, days);
			}
		} else{
			System.out.println("Film " + film + " is not available!");
			System.out.println();
		}

	}
	
	/**
	 * 
	 * Prints out the bill.
	 * 
	 */

	public void total(){
		System.out.println(txt);
		System.out.println("Total price: " + totalSum + " EUR");
		System.out.println("Bonus points remaining: " + this.bonusPoints);
		System.out.println();
		/* Reinitialize variables. */
		txt = new StringBuffer();
		totalSum = 0;
	}
	
	/**
	 * Method for returning a film.
	 * 
	 * @param film the film that the client wants to return
	 * 
	 */

	public void filmReturn(Film film){
		/* If-statement checks if the client has rented the film. */
		if(this.rentedFilm.contains(film)){
			txt.append("Film " + film + " returned. \n");
			this.rentedFilm.remove(film);
			film.setAvailable(true);
		} else{
			txt.append("The client has not rented " + film + ". \n");
		}
	}
	/**
	 * 
	 * Method for returning films late.
	 * 
	 * @param film the film that the client wants to return.
	 * 
	 * @param daysLate indicates how many days the film was returned late
	 * 
	 */
	public void filmReturn(Film film, int daysLate){
		if(daysLate < 0){
			throw new IllegalArgumentException("The number of days can not be a negative number!");
		}
		if(this.rentedFilm.contains(film)){
			this.rentedFilm.remove(film);
			film.setAvailable(true);
			int sum = 0;
			if(film.getType() == FilmType.NEW){
				sum = PREMIUM_PRICE * daysLate;
			} else if(film.getType() == FilmType.REGULAR){
				if(daysLate <= 3){
					sum = BASIC_PRICE;
				} else{
					sum = BASIC_PRICE + BASIC_PRICE * (daysLate - 3);
				}
			} else if(film.getType() == FilmType.OLD){
				if(daysLate <= 5){
					sum = BASIC_PRICE;
				} else{
					sum = BASIC_PRICE + BASIC_PRICE * (daysLate - 5);
				}
			}
			txt.append(film + " " + daysLate + " extra days " + sum + " EUR \n");
			totalSum += sum;
		} else{
			txt.append("The client hasn't rented " + film + ". \n");
		}	
	}

	/**
	 * 
	 * Method for printing out the bill when returning films later than the deadline.
	 */
	public void totalReturn(){
		System.out.println(txt);
		System.out.println("Total late charge: " + totalSum + " EUR");
		System.out.println();
		txt = new StringBuffer();
		totalSum = 0;
	}
}
