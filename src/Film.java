import java.util.ArrayList;

public class Film{
	private String name;
	private FilmType type;
	private boolean isAvailable;
	private static ArrayList<Film> allFilms = new ArrayList<Film>();

	/**
	 * Constructor
	 * 
	 * @param name the name of a new film
	 * 
	 * @param type the type of a new film
	 *            
	 * @param isAvailable if the film is available or not
	 * 
	 */
	public Film(String name, FilmType type, boolean isAvailable) {
		this.name = name;
		this.type = type;
		this.isAvailable = isAvailable;
		Film.allFilms.add(this);
	}

	/**
	 * Constructor
	 * 
	 * @param name the name of a new film
	 * 
	 * @param type the type of a new film
	 * 
	 */
	public Film(String name, FilmType type) {
		this(name, type, true);
	}

	/**
	 * Getter for film name.
	 *
	 * @return returns the name of the film.
	 * 
	 */
	public String getName() {
		return name;
	}

	/**
	 * Getter for film type.
	 *
	 * @return returns the type of the film.
	 * 
	 */
	public FilmType getType() {
		return type;
	}
	
	/**
	 * Method for editing the type of the film.
	 * 
	 * @param type  the type of the film.
	 * 
	 */
	public void setType(FilmType type) {
		this.type = type;
	}
	

	/**
	 * Getter for allFilms ArrayList.
	 *
	 * @return returns ArrayList of all films.
	 * 
	 */
	public static ArrayList<Film> getAllFilms() {
		return allFilms;
	}

	
	/**
	 * Method for checking whether the film is available or not.
	 * 
	 * @return returns the name of the film.
	 * 
	 */
	public boolean isAvailable() {
		return isAvailable;
	}

	/**
	 * Method for editing the availability of the film.
	 * 
	 * @param isAvailable the availability of the film.
	 * 
	 */
	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	@Override
	public String toString() {
		return name + " (" + type + ")";
	}

	/**
	 * 
	 * Method for removing a film.
	 */
	public void removeFilm(){
		Film.allFilms.remove(this);
	}		
}
