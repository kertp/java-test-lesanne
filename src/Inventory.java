import java.util.ArrayList;

public class Inventory {
	
	/**
	 * Method for returning only available films.
	 * 
	 * @return returns ArrayList of available films.
	 */
	public static ArrayList<Film> getAvailableFilms(){
		ArrayList<Film> availableFilms = new ArrayList<Film>();
		for (Film movie : Film.getAllFilms()){
			if(movie.isAvailable()){
				availableFilms.add(movie);
			}
		}
		return availableFilms;
	}
}
