import java.util.ArrayList;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		/* Creating different films. */
		Film outOfAfrica = new Film("Out of Africa", FilmType.OLD,true);
		Film matrix = new Film("Matrix 11", FilmType.NEW, true);
		Film spiderman = new Film("Spider-man", FilmType.REGULAR, true);
		Film spidermanII = new Film("Spider-man 2", FilmType.REGULAR);
		Film avatar = new Film("Avatar", FilmType.REGULAR, false);
		Client a = new Client();
		
		/* Printing all the films. */
		System.out.println("Printing all the films");
		System.out.println(Film.getAllFilms());
		System.out.println();
		
		/* Printing only available films. */
		System.out.println("Printing only available films");
		System.out.println(Inventory.getAvailableFilms());
		System.out.println();
		
		System.out.println(avatar);
		/* Changing type of a film. */
		System.out.println("Changing the type of Avatar to old film");
		avatar.setType(FilmType.OLD);
		System.out.println(avatar);
		
		/* Remove film Avatar */
		System.out.println("Removing film Avatar and printing the list of all the films");
		avatar.removeFilm();
		System.out.println(Film.getAllFilms());
		
		/* Renting films */
		System.out.println();
		System.out.println("Renting films");
		a.lendFilm(matrix, 1);
		a.lendFilm(spiderman, 5);
		a.lendFilm(spidermanII, 2);
		a.lendFilm(outOfAfrica, 7);
		a.total();
		System.out.println("Printing available films");
		System.out.println(Inventory.getAvailableFilms());
		
		/* Returning films */
		System.out.println();
		System.out.println("Returning films");
		a.filmReturn(matrix, 1);
		a.filmReturn(spiderman);
		a.filmReturn(spidermanII);
		a.totalReturn();
		System.out.println("Printing available films after returning them.");
		System.out.println(Inventory.getAvailableFilms());
		
		/* Renting Matrix 11 using bonus points and trying to rent Spider-man (Regular rental) with bonus points. */
		System.out.println();
		System.out.println("Renting Matrix 11 using bonus points and trying to rent Spider-man (Regular rental) with bonus points.");
		a.setBonusPoints(25);
		a.lendFilm(spiderman, 2, 1);
		a.lendFilm(matrix, 2, 1);
		a.total();
		System.out.println("Printing available films");
		System.out.println(Inventory.getAvailableFilms());
	}
}
