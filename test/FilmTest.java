import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class FilmTest {
	Film matrix = new Film("Matrix 11", FilmType.NEW, true);
	Film spiderman = new Film("Spider-man", FilmType.REGULAR, true);
	Film spidermanII = new Film("Spider-man II", FilmType.REGULAR, true);
	Film moneyball = new Film("Moneyball", FilmType.NEW, true);
	Film titanic = new Film("Titanic", FilmType.NEW, false);

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetName() {
		assertEquals("Matrix 11", matrix.getName());
	}

	@Test
	public void testGetType() {
		assertEquals(FilmType.NEW, matrix.getType());
	}

	@Test
	public void testSetType() {
		spiderman.setType(FilmType.OLD);
		assertEquals(FilmType.OLD, spiderman.getType());
	}

	@Test
	public void testIsAvailable() {
		assertEquals(true, matrix.isAvailable());
	}

	@Test
	public void testSetAvailable() {
		ArrayList<Film> temp = Inventory.getAvailableFilms();
		titanic.setAvailable(true);
		ArrayList<Film> temp2 = Inventory.getAvailableFilms();
		temp2.removeAll(temp);
		assertTrue(temp2.size() == 1
				&& titanic == temp2.get(0));
	}

	@Test
	public void testRemoveFilm() {
		ArrayList<Film> temp = new ArrayList<Film>();
		for(Film movie:Film.getAllFilms()){
			temp.add(movie);
		}
		spiderman.removeFilm();
		temp.removeAll(Film.getAllFilms());
		assertTrue(temp.size() == 1 && (spiderman == temp.get(0)));
	}

}
