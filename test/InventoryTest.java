import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class InventoryTest {
	Film spiderman = new Film("Spider-man", FilmType.REGULAR, false);
	Film matrix = new Film("Matrix 11", FilmType.NEW, true);
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetAvailableFilms() {
		assertTrue(Inventory.getAvailableFilms().get(0) == matrix);
	}

}
