import static org.junit.Assert.*;

import java.io.*;
import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ClientTest {
	Film outOfAfrica = new Film("Out of Africa", FilmType.OLD, true);
	Film matrix = new Film("Matrix 11", FilmType.NEW, true);
	Film spiderman = new Film("Spider-man", FilmType.REGULAR, true);
	Film spidermanII = new Film("Spider-man II", FilmType.REGULAR, true);
	Film moneyball = new Film("Moneyball", FilmType.NEW, true);
	Film titanic = new Film("Titanic", FilmType.NEW, false);
	Client a = new Client();
	Client b = new Client();  
	private final ByteArrayOutputStream output = new ByteArrayOutputStream();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception { 
		System.setOut(new PrintStream(this.output));
	}

	@After
	public void tearDown() {
		System.setOut(null);
	}

	@Test
	public void testSetBonusPoints(){
		try{
			a.setBonusPoints(25);
			b.setBonusPoints(-5);
			fail("IllegalArgumentException expected.");
		}
		catch(IllegalArgumentException e){
			assertEquals(25,a.getBonusPoints());
		}
	}

	@Test
	public void testGetBonusPoints() {
		a.setBonusPoints(30);
		assertEquals(30,a.getBonusPoints());
	}

	@Test
	public void testGetRentedFilm() {
		a.lendFilm(matrix, 1);
		assertTrue((a.getRentedFilm().size() == 1) 
				&& a.getRentedFilm().get(0) == matrix);
	}

	@Test
	public void testLendFilmFilmInt() {
		try{
			a.lendFilm(matrix, 1);
			/* Titanic is not available */
			a.lendFilm(titanic, 1);
			a.lendFilm(moneyball, -1);
			fail("IllegalArgumentException expected.");
		}
		catch(IllegalArgumentException e){
			ArrayList<Film> testList = Inventory.getAvailableFilms();
			assertTrue(a.getRentedFilm().get(0) == matrix 
					&& a.getRentedFilm().size() == 1
					&& !(testList.contains(matrix))
					/* Situation when the film is not available. */
					&& (output.toString().contains("Film " + titanic.toString() + " is not available!")));
		}
	}

	@Test
	public void testLendFilmFilmIntInt() {	
		try{
			a.setBonusPoints(50);
			a.lendFilm(matrix, 1,1);
			/* Spider-man is not a new release. */
			a.lendFilm(spiderman, 2, 1);
			/* Titanic is not available. */
			a.lendFilm(titanic, 1, 1);
			a.lendFilm(moneyball, 1, 2);
			fail("IllegalArgumentException expected.");
		}
		catch(IllegalArgumentException e){
			assertTrue((28 == a.getBonusPoints()) 
					&& (output.toString().contains("Film " + titanic.toString() + " is not available!")));
		}
	}

	@Test
	public void testTotal() {
		a.lendFilm(matrix, 1);
		a.lendFilm(spiderman, 5);
		a.total();
		assertTrue((output.toString().contains(matrix.toString())) 
				&& (output.toString().contains(spiderman.toString()))
				&& (output.toString().contains("13 EUR")));
	}

	@Test
	public void testFilmReturnFilm() {
		a.lendFilm(matrix, 1);
		ArrayList<Film> temp = Inventory.getAvailableFilms();
		a.filmReturn(matrix);
		a.filmReturn(outOfAfrica);
		a.totalReturn();
		ArrayList<Film> temp2 = Inventory.getAvailableFilms();
		temp2.removeAll(temp);
		assertTrue((temp2.size() == 1) && (temp2.get(0) == matrix) 
				&& (output.toString().contains("The client has not rented " + outOfAfrica.toString() + ".")));
	}

	@Test
	public void testFilmReturnFilmInt() {
		ArrayList<Film> temp = null;
		ArrayList<Film> temp2 = null;
		try{
			a.lendFilm(matrix, 1);
			temp = Inventory.getAvailableFilms();
			a.filmReturn(matrix, 1);
			a.filmReturn(spiderman, 1);
			a.totalReturn();
			temp2 = Inventory.getAvailableFilms();
			temp2.removeAll(temp);
			a.lendFilm(outOfAfrica, 1);
			/* Illegal argument (-1). */
			a.filmReturn(outOfAfrica, -1);
			fail("IllegalArgumentException expected.");
		}
		catch(IllegalArgumentException e){
			assertTrue((temp2.size() == 1) && (temp2.get(0) == matrix) 
					&& (output.toString().contains("The client hasn't rented " + spiderman.toString())));
		}
	}

	@Test
	public void testTotalReturn() {
		a.lendFilm(matrix, 1);
		a.filmReturn(matrix);
		a.totalReturn();
		assertTrue(output.toString().contains(matrix.toString()));
	}

}
